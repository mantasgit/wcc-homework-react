import React from "react";
import Header from "../components/layout/Header";

export default () => (
  <>
    <Header />
    <section className="content">
      <p>HOME</p>
    </section>
  </>
);
