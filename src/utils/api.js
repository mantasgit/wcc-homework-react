import { MOVIES_API_URL, MOVIES_API_MAX_RESULTS } from "../constants/Api";

export const movies = async (query) => {
  const response = await fetch(`${MOVIES_API_URL}${query}`);
  const results = await response.json();
  if (!results.results || !Array.isArray(results.results)) {
    return [];
  }
  return [...results.results]
    .slice(0, MOVIES_API_MAX_RESULTS)
    .map(({ id, title, popularity, release_date }) => ({
      id,
      title,
      years: new Date(release_date).getFullYear(),
      rating: parseFloat(parseFloat(popularity).toFixed(1)),
    }));
};
