export const MOVIES_API_MIN_CHARS = 3;
export const MOVIES_API_MAX_RESULTS = 8;

export const MOVIES_API_URL = `https://api.themoviedb.org/3/search/movie?api_key=${process.env.REACT_APP_MOVIES_API_KEY}&language=en-US&query=`;

export const API_DEBOUNCE_MS = 222;
