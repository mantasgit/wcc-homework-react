import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import "./styles/style.scss";
import { HOME } from "./constants/Routes";

import Footer from "./components/layout/Footer";

import HomePage from "./pages/Home";

export default () => (
  <>
    <Router>
      <Switch>
        <Route exact path={HOME} component={HomePage} />
        <Redirect from="*" to={HOME} />
      </Switch>
    </Router>
    <Footer />
  </>
);
