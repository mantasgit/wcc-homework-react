import React, { memo } from "react";

export default memo(() => (
  <footer>
    <p>WCC HOMEWORK © MANTAS 2021</p>
  </footer>
));
