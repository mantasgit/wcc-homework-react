import React, { memo, useImperativeHandle, forwardRef, useState } from "react";
import cs from "classnames";

const SearchResult = memo(({ title, rating, years, onSelect }) => (
  <div
    aria-hidden="true"
    onMouseDown={() => onSelect(title)}
    className="result"
  >
    <p className="title">{title}</p>
    <p className="description">{`${rating} Rating, ${years}`}</p>
  </div>
));

const EmptyResponse = ({ title, error }) => (
  <div className="search-results">
    <div className="result no-results">
      <p className={cs("title", { error })}>{title}</p>
    </div>
  </div>
);

const SearchResults = forwardRef((props, ref) => {
  const [results, setResults] = useState([]);

  useImperativeHandle(ref, () => ({
    setResults,
  }));

  if (props.error && props.visible) {
    return <EmptyResponse title={props.error} error />;
  }

  if (results.length === 0 && props.visible) {
    return <EmptyResponse title="No results" />;
  }

  return (
    <div className={cs("search-results", { hidden: !props.visible })}>
      {results.map((item) => (
        <SearchResult
          key={`result_${item.id}`}
          onSelect={props.onSelect}
          title={item.title}
          rating={item.rating}
          years={item.years}
        />
      ))}
    </div>
  );
});

export default memo(SearchResults);
