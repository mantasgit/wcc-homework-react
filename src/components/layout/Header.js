import React, {
  memo,
  useRef,
  useState,
  useCallback,
  useEffect,
  useMemo,
} from "react";
import cs from "classnames";
import SearchResults from "./SearchResults";
import { MOVIES_API_MIN_CHARS, API_DEBOUNCE_MS } from "../../constants/Api";
import { movies as moviesApi } from "../../utils/api";

export default memo(({ defaultValue }) => {
  const [focused, setFocused] = useState(false);
  const [fetchingStatus, setFetchingStatus] = useState({ fetching: false });
  const [searchValue, setSearchValue] = useState(defaultValue || "");
  const inputRef = useRef(null);
  const searchResultRef = useRef(null);
  const debounce = useRef(null);

  const focusInput = useCallback(() => {
    if (!focused) {
      inputRef.current.focus();
      searchForResults(searchValue);
    }
  }, [setSearchValue, focused]);

  const searchForResults = useCallback((query) => {
    if (query.length >= MOVIES_API_MIN_CHARS) {
      if (debounce.current) {
        clearTimeout(debounce.current);
      }
      debounce.current = setTimeout(() => {
        setFetchingStatus(
          () => ({ fetching: true }),
          moviesApi(query)
            .then((res) => {
              if (
                searchResultRef.current &&
                searchResultRef.current.setResults
              ) {
                setFetchingStatus({ fetching: false });
                searchResultRef.current.setResults(res || []);
              }
            })
            .catch(({ message: error }) => {
              setFetchingStatus({ fetching: false, error });
            })
        );
      }, API_DEBOUNCE_MS);
    }
  }, []);

  useEffect(() => searchForResults(searchValue), [searchValue]);

  const onSearchResultSelect = useCallback((title) => setSearchValue(title), [
    setSearchValue,
  ]);

  const setResultsVisible = useMemo(
    () => focused && searchValue.length >= MOVIES_API_MIN_CHARS,
    [searchValue, focused]
  );

  const searchError = useMemo(() => fetchingStatus.error, [
    fetchingStatus.error,
  ]);

  return (
    <header>
      <div className="search-input">
        <div className="movie-icon" />
        <div
          aria-hidden="true"
          onClick={focusInput}
          className={cs("input-wrap", { focused })}
        >
          <div className="movie-icon-in-input">
            <div className="movie-icon-in-input-icon" />
          </div>
          <div className="search-input-content">
            <input
              ref={inputRef}
              onFocus={() => setFocused(true)}
              onBlur={() => setFocused(false)}
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
              type="text"
            />
            <p
              className={cs("placeholder", {
                hidden: !focused && searchValue.length,
                inputNotEmpty: searchValue.length,
              })}
            >
              {fetchingStatus.fetching ? "Loading..." : "Enter movie name"}
            </p>
          </div>
          <div className="animated-borders">
            <div className="border-left">
              <div className="border" />
            </div>
            <div className="border-right">
              <div className="border" />
            </div>
          </div>
          <SearchResults
            ref={searchResultRef}
            error={searchError}
            visible={setResultsVisible}
            onSelect={onSearchResultSelect}
          />
        </div>
      </div>
      <div
        aria-hidden="true"
        onClick={focusInput}
        className={cs("search-button", { hidden: focused })}
      >
        <div className="search-icon" />
      </div>
    </header>
  );
});
