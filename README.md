## Installation

Use the package manager [npm](https://www.npmjs.com/get-npm) to install and run.

```bash
npm install
npm start
```

## Stack
* React
* SASS

## Features

* Frontend
   * Responsive
   * Components transitions
* Backend
   * Debounce
   * Handle error
   * Handle No Results
   * Loading Indicator

## License
[MIT](https://choosealicense.com/licenses/mit/)
